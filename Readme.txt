Research River Discharge Database (R2D2)
contact: simon.munier@meteo.fr

At this time, R2D2 includes discharge data over 1900-2017 from:
- GRDC (global)
- USGS (USA)
- Banque HYDRO (France)
- AFD (Spain)
- ARCTICNET (Northern high latitudes)
- BOM (Australia)
- CHDP (China)
- HYBAM (South America)
- HYDAT (Canada)

To import R2D2 module, just link the R2D2 directory to the current directory:
    ln -s /cnrm/surface/munier/Data/Rivers/R2D2 .
or add R2D2 directory to the Python path
    export PYTHONPATH=/cnrm/surface/munier/Data/Rivers/R2D2:$PYTHONPATH

R2D2 is based on two classes: Station and Catalog

An instance of the Station class describes one specific station, including:
  native id
  data source
  station name and basin name
  coordinates and area
  period of available data

An instance of the Catalog class gathers multiple stations.

Catalogs can be saved and loaded in binary format (npy file), text format (csv)
or netcdf format (nc).
Discharge data can be optionaly saved within the binary or netcdf catalog file.

High resolution basin delineation and mask are available for all stations except
those with missing lat/lon coordinates or drainage area.

A new algorithm has been developed to select the best CTRIP pixel corresponding
to each station.

Next steps:
- better definition of basins name
