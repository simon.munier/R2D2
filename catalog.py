#-*- coding: utf-8 -*-
from .common import *
from .stations import *
from .modules.pyjarowinkler.distance import get_jaro_distance

def default_catalog_file(db='R2D2'):
    db = str.upper(db)
    if db not in known_database:
        raise Exception('Unknown database: {0}'.format(db))
    exec('catalog_file = '+db+'_Station.default_catalog_file')
    return locals()['catalog_file']

def default_station_file(db='R2D2'):
    db = str.upper(db)
    if db not in known_database:
        raise Exception('Unknown database: {0}'.format(db))
    exec('station = '+db+'_Station.default_station_file')
    return locals()['station']

def default_data_file(db='R2D2'):
    db = str.upper(db)
    if db not in known_database:
        raise Exception('Unknown database: {0}'.format(db))
    exec('data_file = '+db+'_Station.default_data_file')
    return locals()['data_file']

class Catalog(object):
    '''
    Create instance of generic Catalog class
      filename: file name with stations to read (can be csv or numpy)
      db      : database from which stations metadata must be extracted

    In filename, first column must be the id of the corresponding database and first line must be fields name.

    filename can also be the file name of a saved catalog (extension *must* be npy)

    If empty_catalog is True, returns an empty catalog (usefull, eg,  with Catalog.from_dataframe)

    Examples:
      ct = Catalog() # load R2D2 catalog
      ct = Catalog(empty_catalog=True) # create a empty Catalog instance
      ct = Catalog('station_file.csv',db='GRDC') # read GRDC stations described by their GRDC id in station_file.csv
      ct = Catalog('user_catalog.npy') # load user_catalog.npy catalog
    '''

    def __init__(self, filename=None, db='R2D2', empty_catalog=False, **kwargs):
        db = str.upper(db)
        if db not in known_database:
            raise Exception('Unknown database: {0}'.format(db))
        self.db = db
        self.st = np.array([])
        self.Nst = 0
        if empty_catalog: return
        if filename is None: filename = default_catalog_file(db)
        if len(filename)>3 and filename[-4:]=='.npy':
            self.load(filename)
            return
        elif len(filename)>2 and filename[-3:]=='.nc':
            self.read_nc(filename,**kwargs)
            return
        station_ids = pd.read_csv(filename,comment='#',delimiter=None,usecols=[0],dtype=str).values.flatten()
        st_list = None
        for stid in tqdm(station_ids):
            st = Station(stid,db=db,st_list=st_list)
            if st_list is None: st_list = st.st_list
            if st.name is not None: self.st = np.append(self.st,st)
        if 'data_file' in self.st[0].__dict__.keys():
            self.data_file = self.st[0].data_file
        self.Nst = len(self.st)
        self.st_list = st_list

    @property
    def id(self): return np.array([st.id for st in self.st])
    @property
    def nat_id(self): return np.array([st.nat_id for st in self.st])
    @property
    def source(self): return np.array([st.source for st in self.st])
    @property
    def name(self): return np.array([st.name for st in self.st])
    @property
    def river(self): return np.array([st.river for st in self.st])
    @property
    def lon(self): return np.array([st.lon for st in self.st],dtype=np.float32)
    @property
    def lat(self): return np.array([st.lat for st in self.st],dtype=np.float32)
    @property
    def area(self): return np.array([st.area for st in self.st],dtype=np.float32)
    @property
    def start(self): return np.array([st.start for st in self.st])
    @property
    def end(self): return np.array([st.end for st in self.st])
    @property
    def length(self): return np.array([st.length for st in self.st],dtype=np.float32)

    def merit(self):
        for st in tqdm(self.st): st.merit()
    @property
    def mrt_lon(self): return np.array([st.mrt_lon for st in self.st])
    @property
    def mrt_lat(self): return np.array([st.mrt_lat for st in self.st])
    @property
    def mrt_dist(self): return np.array([st.mrt_dist for st in self.st])
    @property
    def mrt_area(self): return np.array([st.mrt_area for st in self.st])
    @property
    def mrt_quality(self): return np.array([st.mrt_quality for st in self.st])

    def trip(self,*args,**kwargs):
        for st in tqdm(self.st): st.trip(*args,**kwargs)
    @property
    def trip_lon(self): return np.array([st.trip_lon for st in self.st])
    @property
    def trip_lat(self): return np.array([st.trip_lat for st in self.st])
    @property
    def trip_dist(self): return np.array([st.trip_dist for st in self.st])
    @property
    def trip_area(self): return np.array([st.trip_area for st in self.st])

    def read_data_source(self, start, end, frequency='daily', Ist=None, log=None):
        '''
        Read discharge data for all stations or a selection of stations (Ist)
        '''
        start,end = rdat(start),rdat(end)
        if frequency == 'daily':
            self.dat = pd.DatetimeIndex(start=start,end=end,freq='D')
        elif frequency == 'monthly':
            self.dat = pd.DatetimeIndex(start=start,end=end,freq='MS')
        self.dat = rdat(self.dat)
        if 'Q' not in self.__dict__:
            self.Q = np.nan*np.ones((len(self.dat),self.Nst),dtype='f4')
        if Ist is None: Ist = range(self.Nst)
        if log=='tqdm': Ist = tqdm(Ist)
        for i in Ist:
            if log=='txt': print('{:6d} / {:6d} : {:s}'.format(i+1,len(Ist),self.st[i].name))
            self.st[i].read_data_source(start,end,frequency)
            self.Q[:,i] = self.st[i].Q[:]

    def read_data(self, start, end, frequency='daily', Ist=None):
        '''
        Read discharge data for all stations or a selection of stations (Ist)
        '''
        if Ist is None: Ist = range(self.Nst)
        if frequency == 'daily':
            with open_dataset(self.data_file) as nc:
                nc_id = nc['id'][:]
                ist = [np.where(nc_id==i)[0][0] for i in self.id[Ist]]
                start,end = np.datetime64(rdat(start)),np.datetime64(rdat(end))
                Idat = (nc['time'][:]>=start)*(nc['time'][:]<=end)
                self.dat = nc['time'][Idat].values
                self.Q = nc['data'][ist,Idat].values.T
        elif frequency == 'monthly':
            self.read_data(start,end,frequency='daily')
            dat = self.dat
            data = pd.DataFrame(data=self.Q,index=self.dat)
            data = data.groupby([data.index.year,data.index.month]).mean()
            #data.index = map(lambda d: datetime(d[0],d[1],1),data.index.tolist())
            data.index = [datetime(d[0],d[1],1) for d in data.index.tolist()]
            time_index = pd.DatetimeIndex(start=start,end=end,freq='MS')
            data = data.reindex(time_index)
            self.dat,self.Q = data.index,data.values
        else:
            raise Exception("Only daily or monthly frequency is available with R2D2 data.")
        for i in Ist:
            self.st[i].dat,self.st[i].Q = self.dat,self.Q[:,i]

    def remove_stations(self, stid):
        '''
        Remove selected stations
        '''
        if isinstance(stid,np.ndarray): stid = stid.tolist()
        if not isinstance(stid,list): stid = [stid]
        Ist = [i for i in range(self.Nst) if self.st[i].id not in stid]
        self.st = self.st[Ist]
        self.Nst = len(self.st)
        if 'Q' in self.__dict__:
            self.Q = self.Q[:,Ist]

    def remove_empty_stations(self):
        '''
        Remove stations with only NaN values
        '''
        if 'Q' not in self.__dict__:
            print('Impossible to remove emtpy stations: no discharge data.')
            return
        Ist = np.any(~np.isnan(self.Q),axis=0)
        self.st = self.st[Ist]
        self.Nst = np.sum(Ist)
        self.Q = self.Q[:,Ist]

    def select_stations(self, Ist=None, source=None, nat_id=None, name=None, river=None,
                              zone=None, area_min=None, date_range=None, length_min=None,
                              nb_values=None,jaro_dist=0.75):
        '''
        Make a selection of stations given one or several criteria.
          Ist         : array of selected indices in Catalog.st
          source      : source database
          nat_id      : native id or list of native ids
          zone        : rectangular zone [[upper_left_lon,upper_left_lat],[lower_right_lon,lower_right_lat]]
          area_min    : minimum area
          date_range  : data availability within date_range=[start,end] (start or end may be None)
          length_min  : minimum length of available data (in years)
          nb_values   : minimum number of values
        '''
        if Ist is None:
            Ist = np.ones((self.Nst,),dtype=bool)
        elif len(Ist) != self.Nst:
            tmp = np.zeros((self.Nst,),dtype=bool)
            tmp[Ist] = True
            Ist = tmp
        else:
            Ist = np.array(Ist)
        if source is not None:
            Ist = Ist*(self.source==source)
        if nat_id is not None:
            if isinstance(nat_id,np.ndarray):
                nat_id = nat_id.tolist()
            elif not isinstance(nat_id,list) and not isinstance(nat_id,tuple):
                nat_id = [nat_id]
            Ist = Ist*([nid in nat_id for nid in self.nat_id])
        if name is not None:
            Ist = Ist*([len(stname)>0 and get_jaro_distance(name,stname)>=jaro_dist
                        for stname in self.name])
        if river is not None:
            Ist = Ist*([len(striver)>0 and get_jaro_distance(river,striver)>=jaro_dist
                        for striver in self.river])
        if zone is not None:
            with np.errstate(invalid='ignore'):
                Ist = Ist*(self.lon>=zone[0][0])*(self.lon<=zone[1][0])*\
                          (self.lat<=zone[0][1])*(self.lat>=zone[1][1])
        if area_min is not None:
            Ist = Ist*(self.area>=area_min)
        if date_range is not None:
            if date_range[0] is not None:
                #Ist = Ist*(rdat(self.start)<=rdat(date_range[0]))
                Ist = Ist*(self.start<=rdat(date_range[0],'str'))
            if date_range[1] is not None:
                #Ist = Ist*(np.array(map(rdat,self.end))>=rdat(date_range[1]))
                Ist = Ist*(self.end>=rdat(date_range[1],'str'))
        if length_min is not None:
            Ist = Ist*(self.length>=length_min)
        if nb_values is not None:
            if 'Q' not in self.__dict__.keys():
                print('Please first read data to select stations on nb_values.')
            else:
                Ist = Ist*(np.nansum(~np.isnan(self.Q),axis=0)>=nb_values)
        new_ct = Catalog(db=self.db,empty_catalog=True)
        new_ct.st = self.st[Ist]
        new_ct.Nst = len(new_ct.st)
        if 'st_list' in self.__dict__.keys():
            new_ct.st_list = self.st_list.loc[new_ct.id]
        if 'data_file' in self.__dict__.keys():
            new_ct.data_file = self.data_file
        if 'Q' in self.__dict__:
            new_ct.dat,new_ct.Q = self.dat,self.Q[:,Ist]
        return new_ct

    def to_dataframe(self,alist=None):
        '''
        Convert Catalog to pandas.DataFrame
        '''
        df = []
        for st in self.st: df.append(st.to_series(alist=alist))
        return pd.DataFrame(df)

    def from_dataframe(self,df):
        '''
        Convert pandas.DataFrame to Catalog
        '''
        self.Nst = len(df)
        self.st = np.array([])
        for _,s in df.iterrows():
            st = Station()
            st.from_series(s)
            st.db = self.db
            self.st = np.append(self.st,st)

    def save(self, filename, withQ=False):
        '''
        Save catalog to npy file.
        By default, discharge values are not saved in order to limit catalog size.
        If withQ is True, discharge values are saved.
        '''
        d = dict()
        for k in self.__dict__.keys():
            if not withQ and k in ['dat','Q']: continue
            elif k == 'st':
                d[k] = np.zeros(self.st.shape,dtype=Station)
                for i,st in enumerate(self.st):
                    d[k][i] = Station()
                    for kst in st.__dict__.keys():
                        if kst in ['dat','Q']: continue
                        d[k][i].__dict__[kst] = st.__dict__[kst]
            else:
                d[k] = self.__dict__[k]
        np.save(filename,d)

    def load(self, filename):
        '''
        Load catalog from npy file produced by Catalog.save()
        '''
        try:
            d = np.load(filename)[()]
        except:
            d = np.load(filename,allow_pickle=True)[()]
        self.__dict__.update(d)
        if 'dat' in self.__dict__:
            for i,st in enumerate(self.st):
                st.dat,st.Q = self.dat,self.Q[:,i]

    def to_csv(self, filename):
        '''
        Create csv file with all the stations and metadata in the catalog.
        '''
        self.to_dataframe().to_csv(filename,index=False,encoding='utf-8')

    def create_nc(self, filename, **metadata):
        '''
        Create a netCDF file with metadata and discharge data
        If metadata provided (as keyword arguments):
          each metadata is a tuple of (dtype,attributes_dict,value)
          e.g.: trip_lon=('f4',{'units':'degrees_east'},ct.trip_lon)
        '''
        time_units = 'days since 1900-01-01'
        if self.db=='R2D2':
            source = 'Discharge observations from multiple national and international databases'
        else:
            source = 'Discharge observations from '+self.db
        with Dataset(filename,'w') as nc:
            nc.setncatts({'title':'Research River Discharge Database (R2D2)',
                          'institution':'CNRM/Meteo France',
                          'source':source,
                          'author':'S. Munier (simon.munier@meteo.fr)',
                          'history':'Created on {}'.format(datetime.today().strftime('%Y-%m-%d'))})
            #
            nc.createDimension('station',self.Nst)
            nc.createDimension('time',len(self.dat))
            #
            nc.createVariable('time',np.int32,('time',))
            nc['time'].setncatts({'units':time_units})
            #nc['time'][:] = date2num(rdat(self.dat).to_pydatetime(),time_units)
            nc['time'][:] = date2num(rdat(self.dat),time_units)
            #
            nc.createVariable('id',str,('station',))
            nc['id'].setncatts({'description':'Station id within the database'})
            nc['id'][:] = self.id
            nc.createVariable('nat_id',str,('station',))
            nc['nat_id'].setncatts({'description':'Station id within the original database'})
            nc['nat_id'][:] = self.nat_id
            nc.createVariable('source',str,('station',))
            nc['source'].setncatts({'description':'Name of the original database'})
            nc['source'][:] = self.source
            nc.createVariable('name',str,('station',))
            nc['name'].setncatts({'description':'Station name'})
            nc['name'][:] = self.name
            nc.createVariable('river',str,('station',))
            nc['river'].setncatts({'description':'River or basin name'})
            nc['river'][:] = nan2str(self.river)
            nc.createVariable('lon',np.float32,('station',))
            nc['lon'].setncatts({'description':'Longitude','units':'degrees_east'})
            nc['lon'][:] = self.lon
            nc.createVariable('lat',np.float32,('station',))
            nc['lat'].setncatts({'description':'Latitude','units':'degrees_north'})
            nc['lat'][:] = self.lat
            nc.createVariable('area',np.float32,('station',))
            nc['area'].setncatts({'units':'km2'})
            nc['area'][:] = self.area
            nc.createVariable('start',str,('station',))
            nc['start'].setncatts({'description':'Start date of the available period','units':'days'})
            nc['start'][:] = rdat(self.start,'str')
            nc.createVariable('end',str,('station',))
            nc['end'].setncatts({'description':'End date of the available period','units':'days'})
            nc['end'][:] = rdat(self.end,'str')
            nc.createVariable('length',np.float32,('station',))
            nc['length'].setncatts({'description':'Length of the available period','units':'days'})
            nc['length'][:] = self.length
            # Additional metadata
            if metadata is not None:
                for k,v in metadata.items():
                    nc.createVariable(k,v[0],('station',))
                    nc[k].setncatts(v[1])
                    nc[k][:] = v[2]
            #
            nc.createVariable('data',np.float32,('station','time'))
            nc['data'].setncatts({'description':'Discharge','units':'m3.s-1'})
            nc['data'][:] = self.Q.T

    def read_nc(self, filename, withQ=True):
        '''
        Create Catalog from netCDF file
        '''
        with Dataset(filename) as nc:
            self.data_file = filename
            self.Nst = nc.dimensions['station'].size
            self.st = np.array([Station() for i in range(self.Nst)])
            if withQ:
                self.dat = rdat(num2date(nc['time'][:],nc['time'].units,only_use_cftime_datetimes=False),'str')
                self.Q = nc['data'][:].T
            for k in nc.variables.keys():
                if k in ['time','data']: continue
                if nc[k].dimensions==('station','time'):
                    self.__setattr__(k,nc[k][:].T)
                elif k not in self.__dir__():
                    self.__setattr__(k,nc[k][:])
            for i,st in tqdm(enumerate(self.st)):
                st.db = self.db
                st.data_file = filename
                for k in nc.variables.keys():
                    if k in ['time','data']: continue
                    if nc[k].dimensions==('station','time') and withQ:
                        st.__setattr__(k,self.__getattribute__(k)[:,i])
                    else:
                        st.__setattr__(k,nc[k][i])
                if withQ:
                    st.dat = self.dat
                    st.Q = self.Q[:,i]



