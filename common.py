#-*- coding: utf-8 -*-
import csv
import os,sys
import pdb
import numpy as np
import pandas as pd
from datetime import datetime
from netCDF4 import Dataset,date2num,num2date
from matplotlib import pyplot as plt
from .modules.tqdm import tqdm,trange
#from .modules.xarray import open_dataset
from xarray import open_dataset

#r2d2_dir = '/sx/d0/python2/'
r2d2_dir = '/cnrm/surface/munier/Data/Rivers/'
main_dir = '/cnrm/surface/munier/Data/Rivers/'
#r2d2_dir = '/home/muniers/src/Python/'
#main_dir = '/home/muniers/lustre/Data/Rivers/'

known_database = ['R2D2','GSIM','GRDC','USGS','FRENCH','HYBAM','ARCTICNET','HYDAT','ANA','AFD','CHDP','BOM']
globals().update({db:db for db in known_database})

def rdat(dat,outfmt='datetime'):
    '''
    Convert date array to R2D2 standard date format (datetime.datetime)
    dat can be the following form:
      '2000-01-01'
      [2000,1,1]
      datetime.datetime(2000,1,1,0,0)
    '''
    if type(dat) in [tuple,list,np.ndarray] and len(dat)!=3:
        return np.array([rdat(d,outfmt=outfmt) for d in dat])
    if not isinstance(dat,datetime):
        if type(dat) in [str,np.str_,np.datetime64]: dat = pd.to_datetime(dat)
        elif len(dat) == 3: dat = datetime(*dat)
    if isinstance(dat,pd.Timestamp): dat = dat.to_pydatetime()
    if isinstance(dat,datetime) and outfmt=='str': dat = dat.strftime('%Y-%m-%d')
    return dat

def nan2str(slist,nanchar='-'):
    '''
    Convert NaN values in an str list
    '''
    slist_convert = [nanchar if (isinstance(s,float) and np.isnan(s)) or s is None else s for s in slist]
    if isinstance(slist,np.ndarray):
        return np.array(slist_convert)
    else:
        return slist_convert

def read_bil(filename):
    '''
    Read bil/bsq file with associated header file (hdr)
    '''
    hdrfile = filename.replace('.bil','.hdr').replace('.bsq','.hdr')
    l = pd.read_csv(hdrfile,sep=' ',index_col=0,skipinitialspace=True,header=None).to_dict()[1]
    #l = {k.upper():v for k,v in l.iteritems()}
    l = {k.upper():v for k,v in l.items()}
    lon = np.arange(float(l['ULXMAP']),float(l['ULXMAP'])+int(l['NCOLS'])*float(l['XDIM']),float(l['XDIM']))
    lat = np.arange(float(l['ULYMAP']),float(l['ULYMAP'])-int(l['NROWS'])*float(l['YDIM']),-float(l['YDIM']))
    lon,lat = lon[:int(l['NCOLS'])],lat[:int(l['NROWS'])]
    if l['NBITS']=='1':
        data = np.unpackbits(np.memmap(filename,'u1','r'))[:len(lat)*len(lon)].reshape((len(lat),len(lon)))
    else:
        byteorder = '>' if 'BYTEORDER' in l.keys() and l['BYTEORDER']=='M' else '='
        pixeltype = 'u'
        if 'PIXELTYPE' in l.keys():
            if l['PIXELTYPE'].upper()=='FLOAT': pixeltype = 'f'
            elif l['PIXELTYPE'].upper()=='SIGNEDINT': pixeltype = 'i'
        nbyte = str(int(l['NBITS'])//8)
        dtype = byteorder+pixeltype+nbyte
        data = np.memmap(filename,dtype,'r').reshape((len(lat),len(lon)))
    return lon,lat,data

