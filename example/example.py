# Example of use of the R2D2 module (Research River Discharge Database)
# Change "if True" to "if False" and reciprocally to test the different features.

from R2D2 import *
from cartopy import crs as ccrs, feature as cfeature

zone = [[-10,70],[45,30]]
area_min = 1e3
period = ['2000-01-01','2009-12-31']


if True:
  if True:
    if False:
      # Load R2D2 catalog from csv station file
      ct_all = Catalog(R2D2_Station.default_station_file,db='R2D2')
    else:
      # Load R2D2 catalog from catalog saved file
      ct_all = Catalog(db='R2D2')

    # Select stations from catalog
    ct = ct_all.select_stations(zone=zone,area_min=area_min,date_range=period)

    # Create csv file with selected stations
    ct.to_csv('ex_station_file.csv')

  else:

    # Load R2D2 catalog from csv file with selected stations
    ct = Catalog('ex_station_file.csv',db='R2D2')

  # Read discharge data for selected stations
  ct.read_data(*period)

  # Save catalog with discharge data
  ct.create_nc('ex_catalog.nc')

else:

  # Load catalog with discharge data
  ct = Catalog('ex_catalog.nc')


if True:
  # Locate stations
  plt.figure()
  ax = plt.axes(projection=ccrs.PlateCarree())
  ax.set_extent((zone[0][0],zone[1][0],zone[1][1],zone[0][1]),ccrs.PlateCarree())
  ax.coastlines(resolution='10m')
  ax.add_feature(cfeature.NaturalEarthFeature('physical','rivers_lake_centerlines','10m'),
                facecolor='none',edgecolor='blue',linewidth=1,zorder=2)
  ax.scatter(ct.lon,ct.lat,c=ct.length,s=20,lw=0.25)
  plt.show()

if True:
  # Plot time series
  id_r2d2 = 10
  if True:
    # Get station from catalog
    st = ct.st[id_r2d2+1]
  else:
    # Get station from database
    st = Station(ct.st[id_r2d2+1].id,db='R2D2')
    st.read_data(*period)
  st.plot()
  plt.show()

