#-*- coding: utf-8 -*-
from .common import *
import sqlite3

#===============================================================================
#
#     Any station
#
#===============================================================================
class Station(object):
    '''
    Create instance of generic Station class
      id           : identification code within the database
      nat_id       : native identification code
      db           : database
    Additional keyword arguments
      station_file : complete path to the csv file listing all stations
      data_file    : complete path to the netCDF data file
    '''

    def __init__(self, id=None, nat_id=None, db='R2D2', **kwargs):
        if str.upper(db) not in known_database:
            raise Exception('Unknown database: {0}'.format(db))
        self.id = None
        self.db = None
        self.nat_id = None
        self.source = None
        self.name = None
        self.river = None
        self.country = None
        self.lon,self.lat,self.area = None,None,None
        self.start,self.end,self.length = None,None,None
        self.st_list = None
        if id is None and nat_id is None: return
        self.db = str.upper(db)
        if self.db=='R2D2':
            sttmp = R2D2_Station(id,nat_id,**kwargs)
        elif self.db=='GSIM':
            sttmp = GSIM_Station(id,**kwargs)
        elif self.db=='GRDC':
            sttmp = GRDC_Station(id,**kwargs)
        elif self.db=='USGS':
            sttmp = USGS_Station(id,**kwargs)
        elif self.db=='FRENCH':
            sttmp = FRENCH_Station(id,**kwargs)
        elif self.db=='HYBAM':
            sttmp = HYBAM_Station(id,**kwargs)
        elif self.db=='ARCTICNET':
            sttmp = ARCTICNET_Station(id,**kwargs)
        elif self.db=='HYDAT':
            sttmp = HYDAT_Station(id,**kwargs)
        elif self.db=='ANA':
            sttmp = ANA_Station(id,**kwargs)
        elif self.db=='AFD':
            sttmp = AFD_Station(id,**kwargs)
        elif self.db=='CHDP':
            sttmp = CHDP_Station(id,**kwargs)
        elif self.db=='BOM':
            sttmp = BOM_Station(id,**kwargs)
        self.__dict__.update(sttmp.__dict__)

    def read_data_source(self, start, end, frequency='daily'):
        if self.db=='R2D2':
            self.read_data(start,end,frequency)
            return
        elif self.db=='GRDC':
            sttmp = GRDC_Station(self.id,st_list=self.st_list)
            sttmp.read_data_source(start,end,frequency)
        elif self.db=='USGS':
            sttmp = USGS_Station(self.id,st_list=self.st_list)
            sttmp.read_data_source(start,end,frequency)
        elif self.db=='FRENCH':
            sttmp = FRENCH_Station(self.id,st_list=self.st_list)
            sttmp.read_data_source(start,end,frequency)
        elif self.db=='HYBAM':
            sttmp = HYBAM_Station(self.id,st_list=self.st_list)
            sttmp.read_data_source(start,end,frequency)
        elif self.db=='ARCTICNET':
            sttmp = ARCTICNET_Station(self.id,st_list=self.st_list)
            sttmp.read_data_source(start,end,frequency)
        elif self.db=='HYDAT':
            sttmp = HYDAT_Station(self.id,st_list=self.st_list)
            sttmp.read_data_source(start,end,frequency)
        elif self.db=='ANA':
            sttmp = ANA_Station(self.id,st_list=self.st_list)
            sttmp.read_data_source(start,end,frequency)
        elif self.db=='AFD':
            sttmp = AFD_Station(self.id,st_list=self.st_list)
            sttmp.read_data_source(start,end,frequency)
        elif self.db=='CHDP':
            sttmp = CHDP_Station(self.id,st_list=self.st_list)
            sttmp.read_data_source(start,end,frequency)
        elif self.db=='BOM':
            sttmp = BOM_Station(self.id,st_list=self.st_list)
            sttmp.read_data_source(start,end,frequency)
        else:
            return
        self.__dict__.update({k:sttmp.__dict__[k] for k in ('dat','Q','units')})

    def read_data(self, start, end, frequency='daily'):
        '''
        Read discharge data
          start     : start date
          end       : end date
          frequency : 'daily' or 'monthly'
        '''
        if frequency == 'daily':
            with open_dataset(self.data_file) as nc:
                Ist = np.where(nc.variables['id'][:]==self.id)[0]
                start,end = np.datetime64(rdat(start)),np.datetime64(rdat(end))
                Idat = (nc.variables['time'][:]>=start)*(nc.variables['time'][:]<=end)
                dat = nc.variables['time'][Idat].values
                data = nc.variables['data'][Ist,Idat].values.flatten()
        elif frequency == 'monthly':
            self.read_data(start,end,frequency='daily')
            dat = self.dat
            data = pd.Series(data=self.Q,index=self.dat)
            data = data.groupby([data.index.year,data.index.month]).mean()
            #data.index = map(lambda d: pd.datetime(d[0],d[1],1),data.index.tolist())
            data.index = [pd.datetime(d[0],d[1],1) for d in data.index.tolist()]
            time_index = pd.DatetimeIndex(start=start,end=end,freq='MS')
            data = data.reindex(time_index)
            dat,data = data.index,data.values
        else:
            raise Exception("Only daily or monthly frequency is available with R2D2 data.")
        self.dat = dat
        self.Q = data
        self.units = 'm3.s-1'

    def merit(self):
        '''
        Get estimated coordinates and area from MERIT
        Example:
          st.merit()
          st.mrt_lon     # MERIT longitude
          st.mrt_lat     # MERIT latitude
          st.mrt_dist    # MERIT distance (km)
          st.mrt_area    # MERIT area (km2)
          st.mrt_quality # MERIT search quality
        '''
        if self.db!='R2D2':
            print('MERIT methods only available for R2D2 database')
            return
        basin_file = R2D2_Station.default_basin_file
        names = ['mrt_lon','mrt_lat','mrt_dist','mrt_area','mrt_quality']
        data = pd.read_csv(basin_file,usecols=[3,4,5,7,8],names=names,
                           skiprows=int(self.id),nrows=1)
        self.__dict__.update(data.T.squeeze().to_dict())

    def trip(self,res_str='12D'):
        '''
        Get estimated coordinates and area from TRIP
        Example:
          st.trip()
          st.trip_lon     # TRIP longitude
          st.trip_lat     # TRIP latitude
          st.trip_dist    # TRIP distance (km)
          st.trip_area    # TRIP area (km2)
        '''
        if self.db!='R2D2':
            print('TRIP methods only available for R2D2 database')
            return
        if res_str in ['1D','2D','HD']:
          trip_file = R2D2_Station.default_trip_2D_file
        elif res_str=='12D':
          trip_file = R2D2_Station.default_trip_12D_file
        #names = pd.read_csv(basin_file,header=0,index_col=0,nrows=0)
        names = ['trip_lon','trip_lat','trip_dist','trip_area']
        data = pd.read_csv(trip_file,usecols=[3,4,5,7],names=names,
                           skiprows=int(self.id),nrows=1)
        self.__dict__.update(data.T.squeeze().to_dict())

    def tile(self):
        '''
        Returns 5°x5° tile name (eg n45e005)
        '''
        tile = '{:s}{:02.0f}{:s}{:03.0f}'.format(
          'n' if self.lat>=0 else 's',np.abs(np.floor(self.lat/5.)*5),
          'e' if self.lon>=0 else 'w',np.abs(np.floor(self.lon/5.)*5))
        return tile

    def merit_mask(self, zone=None):
        '''
        Extract basin mask from data base
          lon,lat,mask = st.merit_mask()
        '''
        if self.db!='R2D2':
            print('MERIT methods only available for R2D2 database')
        mask_file = R2D2_Station.default_data_dir+'mask/'+self.tile()+'/'+self.id+'_mask.bsq'
        if not os.path.isfile(mask_file):
            return np.nan,np.nan,np.zeros((0,0))
        lon,lat,mask = read_bil(mask_file)
        if zone is not None:
            res = 1./1200
            new_lon = np.arange(-180+res/2,180,res)
            new_lat = np.arange(90-res/2,-90,-res)
            new_lon = new_lon[(new_lon>=zone[0][0])*(new_lon<=zone[1][0])]
            new_lat = new_lat[(new_lat<=zone[0][1])*(new_lat>=zone[1][1])]
            Ilon = (lon+res/2>=new_lon[0])*(lon-res/2<=new_lon[-1])
            Ilat = (lat-res/2<=new_lat[0])*(lat+res/2>=new_lat[-1])
            Inew_lon = (new_lon+res/2>=lon[0])*(new_lon-res/2<=lon[-1])
            Inew_lat = (new_lat-res/2<=lat[0])*(new_lat+res/2>=lat[-1])
            new_mask = np.zeros((len(new_lat),len(new_lon)),'u1')
            new_mask[np.ix_(Inew_lat,Inew_lon)] = mask[np.ix_(Ilat,Ilon)]
            return new_lon,new_lat,new_mask
        else:
            return lon,lat,mask

    def merit_boundary(self):
        '''
        Extract basin boundary from data base
          bnd = st.merit_boundary # bnd is a 2-column matrix [lon,lat]
        '''
        if self.db!='R2D2':
            print('MERIT methods only available for R2D2 database')
        bnd_file = R2D2_Station.default_data_dir+'boundary/'+self.tile()+'/'+self.id+'_bnd.bin'
        if not os.path.isfile(bnd_file):
            return np.nan,np.nan,np.zeros((0,2))
        bnd = np.fromfile(bnd_file,'<f8').reshape((-1,2))
        return bnd

    def to_series(self,alist=None):
        '''
        Convert Station metadata to pandas Series
        '''
        if alist is None:
          alist = ('id','db','nat_id','source','name','river','country',
                   'lon','lat','area','start','end','length')
        return pd.Series([self.__getattribute__(a) for a in alist],alist)

    def from_series(self,s):
        '''
        Convert pandas Series to Station
        '''
        #for ind,val in s.iteritems(): self.__setattr__(ind,val)
        for ind,val in s.items(): self.__setattr__(ind,val)

    def __str__(self):
        return(self.to_series().__str__())

    def plot(self,*args,**kwargs):
        '''
        Print hydrograph
        '''
        if 'dat' not in self.__dict__.keys() or 'Q' not in self.__dict__.keys():
            print('Warning: no data to plot for station {}.'.format(self.name))
            return
        dat = [rdat(d) for d in self.dat]
        if 'ax' not in kwargs:
          plt.figure()
          ax = plt.axes()
        else:
          ax = kwargs.pop('ax')
        ax.plot(dat,self.Q,*args,**kwargs)
        ax.set_ylabel('discharge (m$^3$/s)')
        ax.set_title('Discharge of {} at station {} ({} - {})'\
                     .format(self.river,self.name,self.source,self.nat_id))



#===============================================================================
#
#     R2D2 stations
#
#===============================================================================
class R2D2_Station(Station):
    '''
    Create R2D2 station
      r2d2_id      : R2D2 identification code
      nat_id       : native identification code
      station_file : complete path to the csv file listing all stations
      data_file    : complete path to the netCDF data file
    '''

    default_station_file = r2d2_dir+'R2D2/R2D2_station_list.csv'
    default_basin_file   = r2d2_dir+'R2D2/R2D2_basin_characteristics.csv'
    default_trip_2D_file = r2d2_dir+'R2D2/R2D2_TRIP_HD_basin.csv'
    default_trip_12D_file= r2d2_dir+'R2D2/R2D2_TRIP_12D_basin.csv'
    default_catalog_file = r2d2_dir+'R2D2/R2D2_catalog.npy'
    default_data_file    = r2d2_dir+'R2D2/data/R2D2_1900-2017_daily.nc'
    default_data_dir     = r2d2_dir+'R2D2/data/'

    def __init__(self, r2d2_id, nat_id=None, station_file=None, data_file=None,
                 extract_all=False, st_list=None, **kwargs):
        if station_file is None: station_file = self.default_station_file
        if data_file is None: data_file = self.default_data_file
        if st_list is None:
            st_list = pd.read_csv(station_file,delimiter=None,dtype=str)
            st_list = st_list.set_index('id')
        if r2d2_id is None and nat_id is not None:
            ist = np.where(st_list['nat_id']==nat_id)[0]
            if len(ist)>0: r2d2_id = st_list.index[ist[0]]
        self.id = str(r2d2_id)
        self.db = 'R2D2'
        self.st_list = st_list
        if self.id in st_list.index:
            st = st_list.loc[self.id]
            if extract_all: self.__dict__.update(st.to_dict())
            self.data_file = data_file
            self.nat_id = st.nat_id
            self.source = st.source
            self.name = st.get('name')
            self.river = st.river
            self.country = st.country
            self.lat = float(st.lat)
            self.lon = float(st.lon)
            self.area = float(st.area)
            self.start = st.start
            self.end = st.end
            self.length = st.length



#===============================================================================
#
#     GSIM stations
#
#===============================================================================
class GSIM_Station(Station):
    '''
    Create GSIM station
      nat_id       : GSIM identification code
      station_file : complete path to the csv file listing all GSIM stations
      data_dir     : complete path to the GSIM data
    '''

    default_station_file = main_dir+'GSIM/GSIM_station_list.csv'
    default_catalog_file = main_dir+'GSIM/GSIM_catalog.npy'
    default_data_dir     = main_dir+'GSIM/'

    def __init__(self, nat_id, station_file=None, data_file=None, data_dir=None,
                 extract_all=False, st_list=None):
        if station_file is None: station_file = self.default_station_file
        if data_file is None: data_file = self.default_data_file
        if data_dir is None: data_dir = self.default_data_dir
        if st_list is None:
            st_list = pd.read_csv(station_file,delimiter=None,dtype=str)
            st_list = st_list.set_index('gsim.no')
        self.id = nat_id
        self.db = 'GSIM'
        self.source = 'GSIM'
        self.st_list = st_list
        if nat_id in st_list.index:
            st = st_list.loc[nat_id]
            if extract_all: self.__dict__.update(st.to_dict())
            self.data_dir = data_dir
            self.nat_id = st['reference.no']
            self.name = st.station
            self.river = st.river
            self.country = st.country
            self.lat = float(st.latitude)
            self.lon = float(st.longitude)
            self.area = float(st.area)
            self.start = st['year.start']+'-01-01'
            self.end = st['year.end']+'-12-31'
            self.length = rdat(self.end).year-rdat(self.start).year+1


#===============================================================================
#
#     GRDC stations
#
#===============================================================================
class GRDC_Station(Station):
    '''
    Create GRDC station
      nat_id       : GRDC identification code
      station_file : complete path to the csv file listing all GRDC stations
      data_file    : complete path to the netCDF data file
      data_dir     : complete path to the GRDC data
    '''

    default_station_file = main_dir+'GRDC/GRDC_station_list.csv'
    default_catalog_file = main_dir+'GRDC/GRDC_catalog.npy'
    default_data_file    = main_dir+'GRDC/GRDC_1900-2020_daily.nc'
    default_data_dir     = main_dir+'GRDC/source/data/'

    def __init__(self, nat_id, station_file=None, data_file=None, data_dir=None,
                 extract_all=False, st_list=None):
        if station_file is None: station_file = self.default_station_file
        if data_file is None: data_file = self.default_data_file
        if data_dir is None: data_dir = self.default_data_dir
        if st_list is None:
            st_list = pd.read_csv(station_file,delimiter=None,dtype=str)
            st_list = st_list.set_index('grdc_no')
        self.id = nat_id
        self.db = 'GRDC'
        self.source = 'GRDC'
        self.st_list = st_list
        if nat_id in st_list.index:
            st = st_list.loc[nat_id]
            if extract_all: self.__dict__.update(st.to_dict())
            self.data_file = data_file
            self.data_dir = data_dir
            self.nat_id = st.nat_id if 'nat_id' in st else nat_id
            self.name = st.station
            self.river = st.river
            self.country = st.country
            self.lat = float(st.lat)
            self.lon = float(st.long)
            self.area = float(st.area)
            self.start = st.t_start+'-01-01'
            self.end = st.t_end+'-12-31'
            self.length = rdat(self.end).year-rdat(self.start).year+1

    def read_data_source(self, start, end, frequency='daily'):
        '''
        Read GRDC data
          start     : start date
          end       : end date
          frequency : 'daily' or 'monthly'
        '''
        start,end = rdat(start),rdat(end)
        if frequency == 'daily':
            try:
                if self.id[0]!='9':
                    fname = '{0}/data_day/{1}_Q_Day.Cmd.txt'.format(self.data_dir, self.id)
                else:
                    fname = '{0}/data_ewa_day/{1}.day'.format(self.data_dir, self.id)
                data = pd.read_csv(fname,comment='#',delimiter=';',usecols=[0,2],index_col=0)
            except:# IOError:
                #raise IOError("No daily data for USGS station {0}.".format(self.id))
                print("No daily data for GRDC station {0}.".format(self.id))
                self.dat = pd.date_range(start=start,end=end,freq='D')
                self.Q = np.nan*np.ones((len(self.dat),))
                self.units = 'm3.s-1'
                return
            data.values[:] = np.where(data<0,np.nan,data.values)
            #data.index = pd.DatetimeIndex(map(lambda s: pd.datetime.strptime(s,'%Y-%m-%d'),data.index))
            data.index = pd.DatetimeIndex([pd.datetime.strptime(s,'%Y-%m-%d') for s in data.index])
            time_index = pd.date_range(start=start,end=end,freq='D')
            data = data.reindex(time_index)
        elif frequency == 'monthly':
            try:
                fname = '{0}/data_mon/{1}_Q_Month.txt'.format(self.data_dir, self.id)
                data = pd.read_csv(fname,comment='#',delimiter=';',usecols=[0,2,3],index_col=0)
                #data.index = pd.DatetimeIndex(map(lambda s: pd.datetime.strptime(s,'%Y-%m-%d'),data.index))
                data.index = pd.DatetimeIndex([pd.datetime.strptime(s,'%Y-%m-%d') for s in data.index])
                data = pd.DataFrame(np.where(data.values[:,1]>=0,data.values[:,1],data.values[:,0]),data.index)
            except (ValueError,IOError):
                self.read_data_source(start,end,frequency='daily')
                data = pd.DataFrame(self.Q,index=self.dat)
                data = data.groupby([data.index.year,data.index.month]).mean()
                #data.index = map(lambda d: pd.datetime(d[0],d[1],1),data.index.tolist())
                data.index = [pd.datetime(d[0],d[1],1) for d in data.index.tolist()]
            except:
                #raise IOError("No data for GRDC station {0}.".format(self.id))
                print("No monthly data for GRDC station {0}.".format(self.id))
                self.dat = pd.date_range(start=start,end=end,freq='MS')
                self.Q = np.nan*np.ones((len(self.dat),))
                self.units = 'm3.s-1'
                return
            time_index = pd.date_range(start=start,end=end,freq='MS')
            data = data.reindex(time_index)
        else:
            raise Exception("Only daily or monthly frequency is available with GRDC data.")
        self.dat = data.index.strftime('%Y-%m-%d')
        self.Q = data.values.flatten().astype('f4')
        self.units = 'm3.s-1'


#===============================================================================
#
#     USGS stations
#
#===============================================================================
class USGS_Station(Station):
    '''
    Create USGS station
      nat_id       : USGS identification code
      station_file : complete path to the csv file listing all USGS stations
      data_file    : complete path to the netCDF data file
      data_dir     : complete path to the USGS data
    '''

    default_station_file = main_dir+'USGS/USGS_station_list.csv'
    default_catalog_file = main_dir+'USGS/USGS_catalog.npy'
    default_data_file    = main_dir+'USGS/USGS_1900-2020_daily.nc'
    default_data_dir     = main_dir+'USGS/source/data/'

    def __init__(self, nat_id, station_file=None, data_file=None, data_dir=None,
                 extract_all=False, st_list=None):
        if station_file is None: station_file = self.default_station_file
        if data_file is None: data_file = self.default_data_file
        if data_dir is None: data_dir = self.default_data_dir
        if st_list is None:
            st_list = pd.read_csv(station_file,delimiter=None,dtype=str)
            st_list = st_list.set_index('site_no')
        self.id = nat_id
        self.db = 'USGS'
        self.nat_id = nat_id
        self.source = 'USGS'
        self.st_list = st_list
        if nat_id in st_list.index:
            st = st_list.loc[nat_id]
            if extract_all: self.__dict__.update(st.to_dict())
            self.data_file = data_file
            self.data_dir = data_dir
            self.name = st.station_nm
            if not isinstance(st.huc_cd,str): self.river = 'unknown'
            else: self._get_basin(st.huc_cd[:4]) # Get USGS Subregion
            self.river = self.river.upper()
            self.country = 'US'
            self.lat = float(st.dec_lat_va)
            self.lon = float(st.dec_long_va)
            self.area = 1.60934**2*float(st.drain_area_va)
            self.start = st.begin_date
            self.end = st.end_date
            self.length = rdat(self.end).year-rdat(self.start).year+1 if st.count_nu!='0' else 0

    def _get_basin(self, huc_cd, basin_file=None):
        if basin_file is None: basin_file = self.data_dir+'../hucs_query.txt'
        basin_list = pd.read_csv(basin_file,comment='#',delimiter='\t',skiprows=[8],dtype=str)
        basin_list = basin_list.set_index('huc_cd')
        if huc_cd not in basin_list.index:
            self.river = 'unknown'
        else:
            self.river = basin_list.loc[huc_cd].huc_nm

    def read_data_source(self, start, end, frequency='daily'):
        '''
        Read USGS data
          start     : start date
          end       : end date
          frequency : 'daily' or 'monthly'
        '''
        start,end = rdat(start),rdat(end)
        try:
            fname = '{0}/USGS_{1}.txt'.format(self.data_dir, self.nat_id)
            data = pd.read_csv(fname,comment='#',delimiter='\t',usecols=[2,3],index_col=0)
        except:# IOError:
            #raise IOError("No daily data for USGS station {0}.".format(self.nat_id))
            print("No daily data for USGS station {0}.".format(self.nat_id))
            self.dat = pd.date_range(start=start,end=end,freq='D')
            self.Q = np.nan*np.ones((len(self.dat),))
            self.units = 'm3.s-1'
            return
        #data.index = pd.DatetimeIndex(map(lambda s: pd.datetime.strptime(s,'%Y-%m-%d'),data.index))
        data.index = pd.DatetimeIndex([pd.datetime.strptime(s,'%Y-%m-%d') for s in data.index])
        if frequency == 'daily':
            time_index = pd.date_range(start=start,end=end,freq='D')
            data = data.reindex(time_index)
        elif frequency == 'monthly':
            data = data.loc[start:end]
            data = data.groupby([data.index.year,data.index.month]).mean()
            #data.index = map(lambda d: pd.datetime(d[0],d[1],1),data.index.tolist())
            data.index = [pd.datetime(d[0],d[1],1) for d in data.index.tolist()]
            time_index = pd.date_range(start=start,end=end,freq='MS')
            data = data.reindex(time_index)
        else:
            raise Exception("Only daily or monthly frequency is available with USGS data.")
        self.dat = data.index
        Q = _tofloat(data.values.flatten())
        self.Q = 0.3048**3*Q
        self.units = 'm3.s-1'


#===============================================================================
#
#     FRENCH stations
#
#===============================================================================
class FRENCH_Station(Station):
    '''
    Create FRENCH station
      nat_id       : FRENCH identification code
      station_file : complete path to the csv file listing all FRENCH stations
      data_file    : complete path to the netCDF data file
      data_dir     : complete path to the FRENCH data
    '''

    default_station_file = main_dir+'FRENCH/FRENCH_station_list.csv'
    default_catalog_file = main_dir+'FRENCH/FRENCH_catalog.npy'
    default_data_file    = main_dir+'FRENCH/FRENCH_1900-2017_daily.nc'
    default_data_dir     = main_dir+'FRENCH/source/FILES/'

    def __init__(self, nat_id, station_file=None, data_file=None, data_dir=None,
                 extract_all=False, st_list=None):
        if station_file is None: station_file = self.default_station_file
        if data_file is None: data_file = self.default_data_file
        if data_dir is None: data_dir = self.default_data_dir
        if st_list is None:
            st_list = pd.read_csv(station_file,delimiter=None,dtype=str)
            st_list = st_list.set_index('station_code')
        self.id = nat_id
        self.db = 'FRENCH'
        self.nat_id = nat_id
        self.source = 'FRENCH'
        self.st_list = st_list
        if nat_id in st_list.index:
            st = st_list.loc[nat_id]
            if extract_all: self.__dict__.update(st.to_dict())
            self.data_file = data_file
            self.data_dir = data_dir
            self.data_file = data_file
            self.name = st.get('name')
            self.river = st.river
            self.country = 'FR'
            self.lat = float(st.lat)
            self.lon = float(st.lon)
            self.area = float(st.area)
            self.start = st.start
            self.end = st.end
            self.length = rdat(self.end).year-rdat(self.start).year+1

    def read_data_source(self, start, end, frequency='daily'):
        '''
        Read FRENCH data
          start     : start date
          end       : end date
          frequency : 'daily' or 'monthly'
        '''
        start,end = rdat(start),rdat(end)
        try:
            fname = '{0}/{1}.obs'.format(self.data_dir, self.nat_id)
            data = pd.read_csv(fname,sep=' ',skiprows=1,skipinitialspace=True,
                               names=['year','month','day','Q'])
        except:# IOError:
            #raise IOError("No daily data for FRENCH station {0}.".format(self.nat_id))
            print("No daily data for FRENCH station {0}.".format(self.nat_id))
            self.dat = pd.date_range(start=start,end=end,freq='D')
            self.Q = np.nan*np.ones((len(self.dat),))
            self.units = 'm3.s-1'
            return
        data.index = pd.DatetimeIndex([pd.datetime(*d) for d in data.values[:,0:3].astype(int)])
        data.drop(['year','month','day'],axis=1,inplace=True)
        if frequency == 'daily':
            time_index = pd.date_range(start=start,end=end,freq='D')
            data = data.reindex(time_index)
        elif frequency == 'monthly':
            data = data.loc[start:end]
            data = data.groupby([data.index.year,data.index.month]).mean()
            #data.index = map(lambda d: pd.datetime(d[0],d[1],1),data.index.tolist())
            data.index = [pd.datetime(d[0],d[1],1) for d in data.index.tolist()]
            time_index = pd.date_range(start=start,end=end,freq='MS')
            data = data.reindex(time_index)
        else:
            raise Exception("Only daily or monthly frequency is available with FRENCH data.")
        self.dat = data.index
        self.Q = data.values.flatten()
        self.Q[self.Q==-999] = np.nan
        self.units = 'm3.s-1'

    def read_data(self, start, end, frequency='daily'):
        if frequency == 'daily':
            with open_dataset(self.data_file) as nc:
                Ist = np.where(nc.variables['id'][:]==self.id)[0]
                start,end = np.datetime64(rdat(start)),np.datetime64(rdat(end))
                Idat = (nc.variables['time'][:]>=start)*(nc.variables['time'][:]<=end)
                dat = nc.variables['time'][Idat].values
                data = nc.variables['data'][Ist,Idat].values.flatten()
        elif frequency == 'monthly':
            data = data.loc[start:end]
            data = data.groupby([data.index.year,data.index.month]).mean()
            #data.index = map(lambda d: pd.datetime(d[0],d[1],1),data.index.tolist())
            data.index = [pd.datetime(d[0],d[1],1) for d in data.index.tolist()]
            time_index = pd.date_range(start=start,end=end,freq='MS')
            data = data.reindex(time_index)
        else:
            raise Exception("Only daily or monthly frequency is available with FRENCH data.")
        self.dat = dat
        self.Q = data
        self.units = 'm3.s-1'




#===============================================================================
#
#     HYBAM stations
#
#===============================================================================
class HYBAM_Station(Station):
    '''
    Create HYBAM station
      st_id        : HYBAM identification code
      station_file : complete path to the csv file listing all HYBAM stations
      data_file    : complete path to the netCDF data file
      data_dir     : complete path to the HYBAM data
    '''

    default_station_file = main_dir+'HYBAM/HYBAM_station_list.csv'
    default_catalog_file = main_dir+'HYBAM/HYBAM_catalog.npy'
    default_data_file    = main_dir+'HYBAM/HYBAM_1900-2017_daily.nc'
    default_data_dir     = main_dir+'HYBAM/source/data/'

    def __init__(self, st_id, station_file=None, data_file=None, data_dir=None,
                 extract_all=False, st_list=None):
        if station_file is None: station_file = self.default_station_file
        if data_file is None: data_file = self.default_data_file
        if data_dir is None: data_dir = self.default_data_dir
        if st_list is None:
            st_list = pd.read_csv(station_file,delimiter=None,dtype=str)
            st_list = st_list.set_index('st_id')
        self.id = st_id
        self.db = 'HYBAM'
        self.source = 'HYBAM'
        self.st_list = st_list
        if st_id in st_list.index:
            st = st_list.loc[st_id]
            if extract_all: self.__dict__.update(st.to_dict())
            self.data_file = data_file
            self.data_dir = data_dir
            self.nat_id = st_id
            self.name = st.station.upper()
            self.river = st.river.upper()
            self.country = st.country.upper()
            self.lat = float(st.lat)
            self.lon = float(st.lon)
            self.area = float(st.area)
            #self.start = st.t_start+'-01-01'
            #self.end = st.t_end+'-12-31'
            #self.length = rdat(self.end).year-rdat(self.start).year+1

    def read_data_source(self, start, end, frequency='daily'):
        '''
        Read HYBAM data
          start     : start date
          end       : end date
          frequency : 'daily' or 'monthly'
        '''
        start,end = rdat(start),rdat(end)
        if frequency == 'daily':
            try:
                fname = '{0}/{1}.csv'.format(self.data_dir, self.id)
                data = pd.read_csv(fname,delimiter=',',usecols=[2,3],index_col=0)
            except:# IOError:
                print("No daily data for HYBAM station {0}.".format(self.id))
                self.dat = pd.date_range(start=start,end=end,freq='D')
                self.Q = np.nan*np.ones((len(self.dat),))
                self.units = 'm3.s-1'
                return
            #data.index = pd.DatetimeIndex(map(lambda s: pd.datetime.strptime(s[:10],'%d/%m/%Y'),data.index))
            data.index = pd.DatetimeIndex([pd.datetime.strptime(s[:10],'%d/%m/%Y') for s in data.index])
            time_index = pd.date_range(start=start,end=end,freq='D')
            data = data.reindex(time_index)
        elif frequency == 'monthly':
            try:
                self.read_data_source(start,end,frequency='daily')
                data = pd.DataFrame(self.Q,index=self.dat)
                data = data.groupby([data.index.year,data.index.month]).mean()
                #data.index = map(lambda d: pd.datetime(d[0],d[1],1),data.index.tolist())
                data.index = [pd.datetime(d[0],d[1],1) for d in data.index.tolist()]
            except:
                #raise IOError("No data for HYBAM station {0}.".format(self.id))
                print("No monthly data for HYBAM station {0}.".format(self.id))
                self.dat = pd.date_range(start=start,end=end,freq='MS')
                self.Q = np.nan*np.ones((len(self.dat),))
                self.units = 'm3.s-1'
                return
            time_index = pd.date_range(start=start,end=end,freq='MS')
            data = data.reindex(time_index)
        else:
            raise Exception("Only daily or monthly frequency is available with HYBAM data.")
        self.dat = data.index.strftime('%Y-%m-%d')
        self.Q = data.values.flatten().astype('f4')
        self.units = 'm3.s-1'


#===============================================================================
#
#     ARCTICNET stations
#
#===============================================================================
class ARCTICNET_Station(Station):
    '''
    Create ARCTICNET station
      st_id        : ARCTICNET identification code
      station_file : complete path to the csv file listing all ARCTICNET stations
      data_file    : complete path to the netCDF data file
      data_dir     : complete path to the ARCTICNET data
    '''

    default_station_file = main_dir+'ARCTICNET/ARCTICNET_station_list.csv'
    default_catalog_file = main_dir+'ARCTICNET/ARCTICNET_catalog.npy'
    default_data_file    = main_dir+'ARCTICNET/ARCTICNET_1900-2017_daily.nc'
    default_data_dir     = main_dir+'ARCTICNET/source/data/'

    def __init__(self, st_id, station_file=None, data_file=None, data_dir=None,
                 extract_all=False, st_list=None):
        if station_file is None: station_file = self.default_station_file
        if data_file is None: data_file = self.default_data_file
        if data_dir is None: data_dir = self.default_data_dir
        if st_list is None:
            st_list = pd.read_csv(station_file,delimiter=None,dtype=str)
            st_list = st_list.set_index('Code')
        self.id = st_id
        self.db = 'ARCTICNET'
        self.source = 'ARCTICNET'
        self.st_list = st_list
        if st_id in st_list.index:
            st = st_list.loc[st_id]
            if extract_all: self.__dict__.update(st.to_dict())
            self.data_file = data_file
            self.data_dir = data_dir
            self.nat_id = st_id
            self.name = st.Name.upper().split(' AT ')[1]
            #self.river = st.Hydrozone.upper()
            self.river = st.Name.upper().split(' AT ')[0]
            self.lat = float(st.Lat)
            self.lon = float(st.Long)
            self.area = float(st.DArea)
            self.start = st.MinOfYear+'-01-01'
            self.end = st.MaxOfYear+'-12-31'
            self.length = rdat(self.end).year-rdat(self.start).year+1

    def read_data_source(self, start, end, frequency='daily'):
        '''
        Read ARCTICNET data
          start     : start date
          end       : end date
          frequency : 'daily' or 'monthly'
        '''
        start,end = rdat(start),rdat(end)
        if frequency == 'daily':
            try:
                fname = '{0}/{1}.csv'.format(self.data_dir, self.id)
                data = pd.read_csv(fname,delimiter=',',index_col=0,header=None)
            except:# IOError:
                print("No daily data for ARCTICNET station {0}.".format(self.id))
                self.dat = pd.date_range(start=start,end=end,freq='D')
                self.Q = np.nan*np.ones((len(self.dat),))
                self.units = 'm3.s-1'
                return
            data.values[:] = np.where(data<0,np.nan,data.values)
            #data.index = pd.DatetimeIndex(map(lambda s: pd.datetime.strptime(s[:10],'%Y-%m-%d'),data.index))
            data.index = pd.DatetimeIndex([pd.datetime.strptime(s[:10],'%Y-%m-%d') for s in data.index])
            time_index = pd.date_range(start=start,end=end,freq='D')
            data = data.reindex(time_index)
        elif frequency == 'monthly':
            try:
                self.read_data_source(start,end,frequency='daily')
                data = pd.DataFrame(self.Q,index=self.dat)
                data = data.groupby([data.index.year,data.index.month]).mean()
                #data.index = map(lambda d: pd.datetime(d[0],d[1],1),data.index.tolist())
                data.index = [pd.datetime(d[0],d[1],1) for d in data.index.tolist()]
            except:
                #raise IOError("No data for ARCTICNET station {0}.".format(self.id))
                print("No monthly data for ARCTICNET station {0}.".format(self.id))
                self.dat = pd.date_range(start=start,end=end,freq='MS')
                self.Q = np.nan*np.ones((len(self.dat),))
                self.units = 'm3.s-1'
                return
            time_index = pd.date_range(start=start,end=end,freq='MS')
            data = data.reindex(time_index)
        else:
            raise Exception("Only daily or monthly frequency is available with ARCTICNET data.")
        self.dat = data.index.strftime('%Y-%m-%d')
        self.Q = data.values.flatten().astype('f4')
        self.units = 'm3.s-1'


#===============================================================================
#
#     HYDAT stations
#
#===============================================================================
class HYDAT_Station(Station):
    '''
    Create HYDAT station
      st_id        : HYDAT identification code
      station_file : complete path to the csv file listing all HYDAT stations
      data_file    : complete path to the netCDF data file
      data_dir     : complete path to the HYDAT data
    '''

    default_station_file = main_dir+'HYDAT/HYDAT_station_list.csv'
    default_catalog_file = main_dir+'HYDAT/HYDAT_catalog.npy'
    default_data_file    = main_dir+'HYDAT/HYDAT_1900-2017_daily.nc'
    default_data_dir     = main_dir+'HYDAT/source/'

    def __init__(self, st_id, station_file=None, data_file=None, data_dir=None,
                 extract_all=False, st_list=None):
        if station_file is None: station_file = self.default_station_file
        if data_file is None: data_file = self.default_data_file
        if st_list is None:
            st_list = pd.read_csv(station_file,dtype=str)
            st_list = st_list.set_index('STATION_NUMBER')
        self.id = st_id
        self.db = 'HYDAT'
        self.source = 'HYDAT'
        self.st_list = st_list
        if st_id in st_list.index:
            st = st_list.loc[st_id]
            if extract_all: self.__dict__.update(st.to_dict())
            self.data_file = data_file
            self.nat_id = st_id
            self.name = st.STATION_NAME
            self.river = None
            self.country = 'CA'
            self.lat = float(st.LATITUDE)
            self.lon = float(st.LONGITUDE)
            self.area = float(st.DRAINAGE_AREA_GROSS)
            self.start = st.YEAR_FROM+'-01-01'
            self.end = st.YEAR_TO+'-12-31'
            self.length = rdat(self.end).year-rdat(self.start).year+1

    def read_data_source(self, start, end, frequency='daily'):
        '''
        Read HYDAT data
          start     : start date
          end       : end date
          frequency : 'daily' or 'monthly'
        '''
        start,end = rdat(start),rdat(end)
        if frequency == 'daily':
            conn = sqlite3.connect(self.data_dir+'Hydat.sqlite3')
            c = conn.cursor()
            fields = 'YEAR,MONTH,'+','.join(['FLOW{}'.format(i) for i in range(1,32)])
            res = c.execute("SELECT {} FROM DLY_FLOWS WHERE STATION_NUMBER='{}';".format(fields,self.id))
            res = np.array(res.fetchall())
            c.close()
            if len(res)==0:
                print("No daily data for HYDAT station {0}.".format(self.id))
                self.dat = pd.date_range(start=start,end=end,freq='D')
                self.Q = np.nan*np.ones((len(self.dat),))
                self.units = 'm3.s-1'
                return
            dat = np.hstack((np.kron(res[:,:2],np.ones((31,1),dtype=int)),\
                             np.tile(np.arange(1,32),res.shape[0]).reshape((-1,1))))
            dat = ['{:04d}-{:02d}-{:02d}'.format(*d) for d in dat]
            data = pd.DataFrame(res[:,2:].flatten(),dat)
            time_index = pd.date_range(start=start,end=end,freq='D').strftime('%Y-%m-%d')
            data = data.reindex(time_index)
        elif frequency == 'monthly':
            self.read_data_source(start,end,frequency='daily')
            data = pd.DataFrame(self.Q,index=self.dat)
            data = data.groupby([data.index.year,data.index.month]).mean()
            #data.index = map(lambda d: pd.datetime(d[0],d[1],1),data.index.tolist())
            data.index = [pd.datetime(d[0],d[1],1) for d in data.index.tolist()]
            time_index = pd.date_range(start=start,end=end,freq='MS')
            data = data.reindex(time_index)
            data.index = data.index.strftime('%Y-%m-%d')
        else:
            raise Exception("Only daily or monthly frequency is available with HYDAT data.")
        self.dat = data.index
        self.Q = data.values.flatten().astype('f4')
        self.units = 'm3.s-1'



#===============================================================================
#
#     ANA stations
#
#===============================================================================
class ANA_Station(Station):
    '''
    Create ANA station
      st_id        : ANA identification code
      station_file : complete path to the csv file listing all ANA stations
      data_file    : complete path to the netCDF data file
      data_dir     : complete path to the ANA data
    '''

    default_station_file = main_dir+'ANA/ANA_station_list.csv'
    default_catalog_file = main_dir+'ANA/ANA_catalog.npy'
    default_data_file    = main_dir+'ANA/ANA_1900-2017_daily.nc'
    default_data_dir     = main_dir+'ANA/source/data/'

    def __init__(self, st_id, station_file=None, data_file=None, data_dir=None,
                 extract_all=False, st_list=None):
        if station_file is None: station_file = self.default_station_file
        if data_file is None: data_file = self.default_data_file
        if data_dir is None: data_dir = self.default_data_dir
        if st_list is None:
            st_list = pd.read_csv(station_file,delimiter=',',dtype=str)
            st_list = st_list.set_index('code')
        self.id = st_id
        self.db = 'ANA'
        self.source = 'ANA'
        self.st_list = st_list
        if st_id in st_list.index:
            st = st_list.loc[st_id]
            if extract_all: self.__dict__.update(st.to_dict())
            self.data_file = data_file
            self.data_dir = data_dir
            self.nat_id = st_id
            self.name = st.get('name')
            self.river = st.river.replace('RIO ','')
            self.country = st.state
            lattmp,lontmp = st.latitude.split(':'),st.longitude.split(':')
            self.lat = float(lattmp[0])+float(lattmp[1])/60.+float(lattmp[2])/3600.
            self.lon = float(lontmp[0])+float(lontmp[1])/60.+float(lontmp[2])/3600.
            self.area = float(st.dr_area)
            #self.start = st.MinOfYear+'-01-01'
            #self.end = st.MaxOfYear+'-12-31'
            #self.length = st.CountOfYear

    def read_data_source(self, start, end, frequency='daily'):
        '''
        Read ANA data
          start     : start date
          end       : end date
          frequency : 'daily' or 'monthly'
        '''
        start,end = rdat(start),rdat(end)
        if frequency == 'daily':
            try:
                fname = '{0}/{1}.csv'.format(self.data_dir, self.id)
                data = pd.read_csv(fname,delimiter=';',usecols=[2]+list(range(16,47)),
                                   index_col=0,decimal=',',header=15)
                data.drop_duplicates(inplace=True)
            except:# IOError:
                print("No daily data for ANA station {0}.".format(self.id))
                self.dat = pd.date_range(start=start,end=end,freq='D')
                self.Q = np.nan*np.ones((len(self.dat),))
                self.units = 'm3.s-1'
                return
            years  = np.array([int(s[6:]) for s in data.index]).reshape((-1,1))
            months = np.array([int(s[3:5]) for s in data.index]).reshape((-1,1))
            dat = np.hstack((np.kron(np.hstack((years,months)),np.ones((31,1),dtype=int)),\
                             np.tile(np.arange(1,32),len(data)).reshape((-1,1))))
            dat = ['{:04d}-{:02d}-{:02d}'.format(*d) for d in dat]
            data = pd.DataFrame(data.values.flatten(),dat)
            time_index = pd.date_range(start=start,end=end,freq='D').strftime('%Y-%m-%d')
            try:
                data = data.reindex(time_index)
            except:
                data = data.reset_index().drop_duplicates('index').set_index('index').reindex(time_index)
        elif frequency == 'monthly':
            try:
                self.read_data_source(start,end,frequency='daily')
                data = pd.DataFrame(self.Q,index=self.dat)
                data = data.groupby([data.index.year,data.index.month]).mean()
                #data.index = map(lambda d: pd.datetime(d[0],d[1],1),data.index.tolist())
                data.index = [pd.datetime(d[0],d[1],1) for d in data.index.tolist()]
            except:
                #raise IOError("No data for ANA station {0}.".format(self.id))
                print("No monthly data for ANA station {0}.".format(self.id))
                self.dat = pd.date_range(start=start,end=end,freq='MS')
                self.Q = np.nan*np.ones((len(self.dat),))
                self.units = 'm3.s-1'
                return
            time_index = pd.date_range(start=start,end=end,freq='MS')
            data = data.reindex(time_index).strftime('%Y-%m-%d')
        else:
            raise Exception("Only daily or monthly frequency is available with ANA data.")
        self.dat = data.index
        self.Q = data.values.flatten().astype('f4')
        self.units = 'm3.s-1'


#===============================================================================
#
#     AFD stations
#
#===============================================================================
class AFD_Station(Station):
    '''
    Create AFD station
      st_id        : AFD identification code
      station_file : complete path to the csv file listing all AFD stations
      data_file    : complete path to the netCDF data file
      data_dir     : complete path to the AFD data
    '''

    default_station_file = main_dir+'AFD/AFD_station_list.csv'
    default_catalog_file = main_dir+'AFD/AFD_catalog.npy'
    default_data_file    = main_dir+'AFD/AFD_1900-2017_daily.nc'
    default_data_dir     = main_dir+'AFD/source/data/'

    def __init__(self, st_id, station_file=None, data_file=None, data_dir=None,
                 extract_all=False, st_list=None):
        if station_file is None: station_file = self.default_station_file
        if data_file is None: data_file = self.default_data_file
        if data_dir is None: data_dir = self.default_data_dir
        if st_list is None:
            st_list = pd.read_csv(station_file,delimiter=',',dtype=str)
            st_list = st_list.set_index('code')
        self.id = st_id
        self.db = 'AFD'
        self.source = 'AFD'
        self.st_list = st_list
        if st_id in st_list.index:
            st = st_list.loc[st_id]
            if extract_all: self.__dict__.update(st.to_dict())
            self.data_file = data_file
            self.data_dir = data_dir
            self.nat_id = st_id
            self.name = st.get('name')
            self.river = st.river
            self.country = 'ES'
            self.lat = float(st.lat)
            self.lon = float(st.lon)
            self.area = float(st.area)
            try:
                self.start = st.start+'-01-01'
                self.end = st.end+'-12-31'
                self.length = rdat(self.end).year-rdat(self.start).year+1
            except:
                pass

    def read_data_source(self, start, end, frequency='daily'):
        '''
        Read AFD data
          start     : start date
          end       : end date
          frequency : 'daily' or 'monthly'
        '''
        start,end = rdat(start),rdat(end)
        if frequency == 'daily':
            try:
                fname = '{0}/{1}.txt'.format(self.data_dir, self.id)
                data = pd.read_csv(fname,delimiter=' ',usecols=[1,3],index_col=0,skiprows=6,\
                                   skipinitialspace=True,header=None)
                data.values[data.values==-100] = np.nan
            except:# IOError:
                print("No daily data for AFD station {0}.".format(self.id))
                self.dat = pd.date_range(start=start,end=end,freq='D')
                self.Q = np.nan*np.ones((len(self.dat),))
                self.units = 'm3.s-1'
                return
            #data.index = pd.date_range(map(lambda s: pd.datetime.strptime(s,'%d/%m/%Y'),data.index))
            #data.index = map(lambda s: '-'.join((s[6:],s[3:5],s[:2])),data.index)
            data.index = ['-'.join((s[6:],s[3:5],s[:2])) for s in data.index]
            time_index = pd.date_range(start=start,end=end,freq='D').strftime('%Y-%m-%d')
            data = data.reindex(time_index)
        elif frequency == 'monthly':
            try:
                self.read_data_source(start,end,frequency='daily')
                data = pd.DataFrame(self.Q,index=self.dat)
                data = data.groupby([data.index.year,data.index.month]).mean()
                #data.index = map(lambda d: pd.datetime(d[0],d[1],1),data.index.tolist())
                data.index = [pd.datetime(d[0],d[1],1) for d in data.index.tolist()]
            except:
                #raise IOError("No data for AFD station {0}.".format(self.id))
                print("No monthly data for AFD station {0}.".format(self.id))
                self.dat = pd.date_range(start=start,end=end,freq='MS')
                self.Q = np.nan*np.ones((len(self.dat),))
                self.units = 'm3.s-1'
                return
            time_index = pd.date_range(start=start,end=end,freq='MS')
            data = data.reindex(time_index).strftime('%Y-%m-%d')
        else:
            raise Exception("Only daily or monthly frequency is available with AFD data.")
        self.dat = data.index
        self.Q = data.values.flatten().astype('f4')
        self.units = 'm3.s-1'


#===============================================================================
#
#     CHDP stations
#
#===============================================================================
class CHDP_Station(Station):
    '''
    Create CHDP station
      st_id        : CHDP identification code
      station_file : complete path to the csv file listing all CHDP stations
      data_file    : complete path to the netCDF data file
      data_dir     : complete path to the CHDP data
    '''

    default_station_file = main_dir+'CHDP/CHDP_station_list.csv'
    default_catalog_file = main_dir+'CHDP/CHDP_catalog.npy'
    default_data_file    = main_dir+'CHDP/CHDP_1900-2017_daily.nc'
    default_data_dir     = main_dir+'CHDP/source/data/'

    def __init__(self, st_id, station_file=None, data_file=None, data_dir=None,
                 extract_all=False, st_list=None):
        if station_file is None: station_file = self.default_station_file
        if data_file is None: data_file = self.default_data_file
        if data_dir is None: data_dir = self.default_data_dir
        if st_list is None:
            st_list = pd.read_csv(station_file,delimiter=',',dtype=str)
            st_list = st_list.set_index('station_number')
        self.id = st_id
        self.db = 'CHDP'
        self.source = 'CHDP'
        self.st_list = st_list
        if st_id in st_list.index:
            st = st_list.loc[st_id]
            if extract_all: self.__dict__.update(st.to_dict())
            self.data_file = data_file
            self.data_dir = data_dir
            self.nat_id = st_id
            self.name = st.station_name.upper()
            self.river = st.river_name.upper()
            self.country = 'CN'
            self.lat = float(st.latitude)
            self.lon = float(st.longitude)
            self.area = float(st.area)

    def read_data_source(self, start, end, frequency='daily'):
        '''
        Read CHDP data
          start     : start date
          end       : end date
          frequency : 'daily' or 'monthly'
        '''
        start,end = rdat(start),rdat(end)
        if frequency == 'daily':
            try:
                fname = '{0}/station_{1}.csv'.format(self.data_dir, self.id)
                data = pd.read_csv(fname,delimiter=',',index_col=0)
                data.values[data.values==0] = np.nan
            except:# IOError:
                print("No daily data for CHDP station {0}.".format(self.id))
                self.dat = pd.date_range(start=start,end=end,freq='D')
                self.Q = np.nan*np.ones((len(self.dat),))
                self.units = 'm3.s-1'
                return
            #data.index = pd.date_range(map(lambda s: pd.datetime.strptime(s,'%d/%m/%Y'),data.index))
            time_index = pd.date_range(start=start,end=end,freq='D').strftime('%Y-%m-%d')
            data = data.reindex(time_index)
        elif frequency == 'monthly':
            try:
                self.read_data_source(start,end,frequency='daily')
                data = pd.DataFrame(self.Q,index=self.dat)
                data = data.groupby([data.index.year,data.index.month]).mean()
                #data.index = map(lambda d: pd.datetime(d[0],d[1],1),data.index.tolist())
                data.index = [pd.datetime(d[0],d[1],1) for d in data.index.tolist()]
            except:
                #raise IOError("No data for CHDP station {0}.".format(self.id))
                print("No monthly data for CHDP station {0}.".format(self.id))
                self.dat = pd.date_range(start=start,end=end,freq='MS')
                self.Q = np.nan*np.ones((len(self.dat),))
                self.units = 'm3.s-1'
                return
            time_index = pd.date_range(start=start,end=end,freq='MS')
            data = data.reindex(time_index).strftime('%Y-%m-%d')
        else:
            raise Exception("Only daily or monthly frequency is available with CHDP data.")
        self.dat = data.index
        self.Q = data.values.flatten().astype('f4')
        self.units = 'm3.s-1'


#===============================================================================
#
#     BOM stations
#
#===============================================================================
class BOM_Station(Station):
    '''
    Create BOM station
      st_id        : BOM identification code
      station_file : complete path to the csv file listing all BOM stations
      data_file    : complete path to the netCDF data file
      data_dir     : complete path to the BOM data
    '''

    default_station_file = main_dir+'BOM/BOM_station_list.csv'
    default_catalog_file = main_dir+'BOM/BOM_catalog.npy'
    default_data_file    = main_dir+'BOM/BOM_1900-2017_daily.nc'
    default_data_dir     = main_dir+'BOM/source/data/'

    def __init__(self, st_id, station_file=None, data_file=None, data_dir=None,
                 extract_all=False, st_list=None):
        if station_file is None: station_file = self.default_station_file
        if data_file is None: data_file = self.default_data_file
        if data_dir is None: data_dir = self.default_data_dir
        if st_list is None:
            st_list = pd.read_csv(station_file,delimiter=',',dtype=str)
            st_list = st_list.set_index('Station number')
        self.id = st_id
        self.db = 'BOM'
        self.source = 'BOM'
        self.st_list = st_list
        if st_id in st_list.index:
            st = st_list.loc[st_id]
            if extract_all: self.__dict__.update(st.to_dict())
            self.data_file = data_file
            self.data_dir = data_dir
            self.nat_id = st_id
            self.name = st['Station name'].upper()
            self.river = ''
            self.country = 'AU'
            self.lat = float(st.Latitude)
            self.lon = float(st.Longitude)

    def read_data_source(self, start, end, frequency='daily'):
        '''
        Read BOM data
          start     : start date
          end       : end date
          frequency : 'daily' or 'monthly'
        '''
        start,end = rdat(start),rdat(end)
        if frequency == 'daily':
            try:
                fname = '{0}/{1}.csv'.format(self.data_dir, self.id)
                data = pd.read_csv(fname,delimiter=',',usecols=[0,1],index_col=0,
                                   comment='#',header=None)
            except:# IOError:
                print("No daily data for BOM station {0}.".format(self.id))
                self.dat = pd.date_range(start=start,end=end,freq='D')
                self.Q = np.nan*np.ones((len(self.dat),))
                self.units = 'm3.s-1'
                return
            data.values[:] = np.where(data.values==-999,np.nan,data.values.astype('f4'))
            #data.index = map(lambda s: s[:10],data.index)
            data.index = [s[:10] for s in data.index]
            time_index = pd.date_range(start=start,end=end,freq='D').strftime('%Y-%m-%d')
            data = data.reindex(time_index)
        elif frequency == 'monthly':
            try:
                self.read_data_source(start,end,frequency='daily')
                data = pd.DataFrame(self.Q,index=self.dat)
                data = data.groupby([data.index.year,data.index.month]).mean()
                #data.index = map(lambda d: pd.datetime(d[0],d[1],1),data.index.tolist())
                data.index = [pd.datetime(d[0],d[1],1) for d in data.index.tolist()]
            except:
                #raise IOError("No data for BOM station {0}.".format(self.id))
                print("No monthly data for BOM station {0}.".format(self.id))
                self.dat = pd.date_range(start=start,end=end,freq='MS')
                self.Q = np.nan*np.ones((len(self.dat),))
                self.units = 'm3.s-1'
                return
            time_index = pd.date_range(start=start,end=end,freq='MS')
            data = data.reindex(time_index).strftime('%Y-%m-%d')
        else:
            raise Exception("Only daily or monthly frequency is available with BOM data.")
        self.dat = data.index
        self.Q = data.values.flatten().astype('f4')
        self.units = 'm3.s-1'


#===============================================================================
#
#     Nested functions
#
#===============================================================================

def _tofloat(v):
    '''
    Convert array to float values
    np.nan for non numeric values
    '''
    f = lambda w: np.float(w) if not isinstance(w,str) else \
                 (np.float(w) if np.char.isnumeric(str(w)) else np.nan)
    return np.float32(np.vectorize(f)(v))




